#!/usr/bin/env python3
# Compatibility: python3.6
##
# Copyright (C)  2018 Jan Chren (rindeal)
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only
##

from distutils.core import setup
import os
import sys


# make sure local modules are imported
sys.path.insert(0, os.path.join(os.path.dirname(__file__), "lib"))
from rindeal.subprocess.keepalive._pkg_ import PkgMetadata


setup(
	# >> required fields
	name=PkgMetadata.name,
	version=PkgMetadata.version,
	description=PkgMetadata.summary,
	url=PkgMetadata.url,

	# >> creator section
	author=PkgMetadata.author,
	author_email=PkgMetadata.email,
	license=PkgMetadata.licence,

	# >> stuff to actually do
	package_dir={'': 'lib'},
	packages=("rindeal.subprocess.keepalive",),
	scripts=("bin/subprocess-keepalive",),
)
