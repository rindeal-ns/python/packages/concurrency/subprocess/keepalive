# Python-Version-Compatibility: 3.6
##
# Copyright (C)  2018 Jan Chren (rindeal)
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only
##

__all__ = ('WATCH_STDOUT', 'WATCH_STDERR', 'run')


import copy
import itertools
import os
import select
import subprocess
import sys
import time
import typing


# these constants are used in `keepalive_watch` argument of `run()` function
# to select subprocess' stdout/stderr
WATCH_STDOUT = -100
WATCH_STDERR = -101


_Number = typing.Union[int, float]


class _PipeFd:
	"""
	Provides handy closing of file descriptors for pipes, but can be also used elsewhere
	"""

	_fd: int

	def __init__(self, fd: int =-1):
		self._fd = fd

	def close(self):
		if self._fd < 0:
			return

		os.close(self._fd)
		self._fd = -1

	def __int__(self):
		return self._fd

	def __del__(self):
		self.close()

	def __repr__(self):
		return f"{self.__class__.__name__}({self._fd})"

	def __str__(self):
		return str(self._fd)


class _Pipe:
	"""
	Wrapper around os.pipe()
	"""

	r: _PipeFd
	w: _PipeFd

	def __init__(self):
		r, w = os.pipe()
		self.r = _PipeFd(r)
		self.w = _PipeFd(w)

	def __repr__(self):
		return f"{self.__class__.__name__}(r={self.r}, w={self.w})"

	def close(self):
		self.r.close()
		self.w.close()

	def __del__(self):
		self.close()


class _StdouterrPiping:
	"""
	Holds all the file descriptors used in the processing of a single stream

	It's used like this:
		1. subprocess receives `self.proc.w` to write data into
		2. _StdouterrPipingHandler receives the data on `self.proc.r`
		3. _StdouterrPipingHandler bumps the last activity timestamp
		4. _StdouterrPipingHandler passes the data to `self.user`
	"""

	proc: _Pipe
	user: int

	def __init__(self, user: int):
		self.proc = _Pipe()
		self.user = user


class _Timestamp:
	_ts: float

	def __init__(self):
		self._ts = time.time()

	def bump(self):
		self._ts = time.time()

	def elapsed(self):
		return time.time() - self._ts


class _Epoll:
	"""
	"Enhanced" epoll, sadly `select.epoll` cannot be a base type so just a wrapper

	Provides context manager, `__del__()` and a few handy methods
	"""

	_event_handlers: typing.Dict[int, '_EpollEventHandler']
	_epoll: select.epoll
	_registered_fds: set

	def __init__(self):
		self._event_handlers = dict()
		self._registered_fds = set()

		self._epoll = select.epoll()

		# > "inherits":
		self.close = self._epoll.close
		self.fileno = self._epoll.fileno
		self.modify = self._epoll.modify

	def poll(
			self,
			timeout: typing.Optional[_Number] =None,
			maxevents: int =-1
	) -> typing.Sequence[typing.Tuple[int, int]]:
		return self._epoll.poll(timeout, maxevents)

	@property
	def closed(self):
		return self._epoll.closed

	def register(self, fd: int, mask: int) -> '_HandlerRegistratorProxy':
		self._epoll.register(fd, mask)
		self._registered_fds.add(fd)

		class _HandlerRegistratorProxy(typing.NamedTuple):
			epoll: '_Epoll'
			fd: int

			def with_handler(self, func: typing.Callable, args: typing.Tuple[typing.Any, ...] =()):
				self.epoll.set_handler(self.fd, func, args)

		return _HandlerRegistratorProxy(self, fd)

	def set_handler(
			self,
			fd: int,
			handler_cls: typing.Type['_EpollEventHandler'],
			custom_init_args: typing.Optional[typing.Tuple[typing.Any, ...]] =None,
			custom_init_kwargs: typing.Optional[typing.Dict[str, typing.Any]] =None,
	) -> None:
		if fd not in self._registered_fds:
			raise ValueError("fd is not registered")

		handler = handler_cls(self, fd)
		if custom_init_args or custom_init_kwargs:
			handler.custom_init(*custom_init_args, **custom_init_kwargs)
		self._event_handlers[fd] = handler

	def dispatch_event(self, fd: int, event: int) -> None:
		if fd not in self._event_handlers:
			return
		self._event_handlers[fd].handle(event)

	def unregister(self, fd: int) -> None:
		if fd in self._registered_fds:
			self._registered_fds.remove(fd)
		else:
			return

		if fd in self._event_handlers:
			del(self._event_handlers[fd])

		self._epoll.unregister(fd)

	def __len__(self) -> int:
		return len(self._registered_fds)

	def __contains__(self, fd: int) -> bool:
		return fd in self._registered_fds

	def __iter__(self) -> 'set_iterator':
		return self._registered_fds.__iter__()

	def __enter__(self):
		return self

	def __exit__(self, *_) -> None:
		self.close()

	def __del__(self) -> None:
		self.close()


class _EpollEventHandler:
	"""
	Base class used to implement epoll event handlers
	"""

	_epoll: _Epoll
	_fd: int

	def __init__(self, epoll: _Epoll, fd: int) -> None:
		self._epoll = epoll
		self._fd = fd

	def custom_init(self, *_, **__) -> None:
		pass

	def handle(self, ev_types: int) -> None:
		raise NotImplementedError

	def _unregister(self) -> None:
		self._epoll.unregister(self._fd)


class _InputHandler(_EpollEventHandler):
	"""
	Handles `input` feature, which feeds data into the stdin of the subprocess
	"""

	_bytes_written: int = 0
	_stdin_io: typing.IO
	_input: bytes

	def custom_init(self, stdin_io: typing.IO, input_: bytes):
		self._stdin_io = stdin_io
		self._input = input_

	def handle(self, ev_types: int):
		# handle HUP first as it doesn't make sense to beat a dead horse
		if ev_types & select.EPOLLHUP:
			self._unregister()

		elif ev_types & select.EPOLLOUT:
			self._bytes_written += os.write(self._fd, self._input[self._bytes_written:])
			if self._bytes_written >= len(self._input):
				self._unregister()

	def __del__(self):
		self._stdin_io.close()


class _StdouterrPipingHandler(_EpollEventHandler):
	"""
	Handles proxying and last activity timer bumping on WATCH_STD* streams
	"""

	_piping: _StdouterrPiping
	_last_activity_ts: _Timestamp

	def custom_init(self, piping: _StdouterrPiping, last_activity_ts: _Timestamp):
		self._piping = piping
		self._last_activity_ts = last_activity_ts

	def handle(self, ev_types: int):
		if ev_types & select.EPOLLIN:
			buf = os.read(self._fd, select.PIPE_BUF)

			# data available
			if len(buf):
				# WARNING: this might block!
				os.write(self._piping.user, buf)
				self._last_activity_ts.bump()

			# EOF
			else:
				# this never happens, but nevermind
				self._unregister()

		elif ev_types & select.EPOLLHUP:
			self._unregister()

	def __del__(self):
		self._piping.proc.close()


class _NonPipingWatcherHandler(_EpollEventHandler):
	"""
	Handles basic last activity timer bumping on unknown fds
	"""

	_last_activity_ts: _Timestamp

	def custom_init(self, last_activity_ts: _Timestamp):
		self._last_activity_ts = last_activity_ts

	def handle(self, ev_types: int):
		if ev_types & select.EPOLLIN:
			self._last_activity_ts.bump()

		elif ev_types & select.EPOLLHUP:
			self._unregister()


class _BufferingHandler(_EpollEventHandler):
	"""
	Handles stdout/stderr stream capturing
	"""

	_fh: typing.IO
	_buf: typing.List[bytes]

	def custom_init(self, fh: typing.IO, buf: typing.List[bytes]):
		self._fh = fh
		self._buf = buf

	def handle(self, ev_types: int):
		if ev_types & select.EPOLLIN:
			data = os.read(self._fd, select.PIPE_BUF)
			if data:
				self._buf.append(data)
			else:
				self._unregister()
		else:
			self._unregister()

	def __del__(self):
		self._fh.close()


class _ChildExitHandler(_EpollEventHandler):
	_proc: subprocess.Popen

	def custom_init(self, proc: subprocess.Popen):
		self._proc = proc

	def handle(self, ev_types: int):
		self._proc.poll()
		self._unregister()


class _Buffers:
	"""
	Used to implement stdout/stderr stream capturing
	"""

	stdout: typing.List[bytes]
	stderr: typing.List[bytes]

	def __init__(self):
		self.stdout = list()
		self.stderr = list()

	def get_all(self) -> typing.Tuple[typing.Optional[bytes], typing.Optional[bytes]]:
		return tuple(b''.join(x) if x else None for x in (self.stdout, self.stderr))


def run(
		args: typing.Union[typing.Sequence, str],

		input: typing.Optional[bytes] =None,
		capture_output: bool = False,
		timeout: typing.Optional[_Number] =None,
		check: bool =False,

		keepalive_watch: typing.Sequence[int] =(WATCH_STDOUT, WATCH_STDERR),
		keepalive_interval: _Number =60,
		keepalive_packet: bytes =b'\r\033[K',
		keepalive_write: typing.Sequence[int] =(sys.stderr.fileno(),),

		**popenkwargs: typing.Any
):
	"""
	`subprocess.run()` drop-in replacement with keepalive features

	If `keepalive_watch` streams won't output anything in less than `keepalive_interval`
	seconds, `keepalive_packet` will be written to `keepalive_write` streams.

	Useful eg. on CI servers which monitor log output for activity to keep job running.

	Text mode is not supported for simplicity.

	epoll-based solution, no threads involved.

	:param popenargs:
	:param input:
	:param capture_output:
	:param timeout:
	:param check:

	:param keepalive_watch: Must not be a regular file as epoll doesn't work with regular files.
	:param keepalive_interval:
	:param keepalive_packet:
	:param keepalive_write:

	:param popenkwargs:
	:return:
	"""
	# >> validate arguments
	if any(x in popenkwargs for x in ('encoding', 'errors', 'text', 'universal_newlines')):
		raise NotImplementedError('Text mode is not supported')

	# > https://github.com/python/cpython/blob/3.7/Lib/subprocess.py#L454-L457
	if input is not None:
		if 'stdin' in popenkwargs:
			raise ValueError('stdin and input arguments may not be used at the same time.')
		popenkwargs['stdin'] = subprocess.PIPE

	# > https://github.com/python/cpython/blob/3.7/Lib/subprocess.py#L459-L464
	if capture_output:
		if ('stdout' in popenkwargs) or ('stderr' in popenkwargs):
			raise ValueError('stdout and stderr arguments may not be used with capture_output.')
		popenkwargs['stdout'] = subprocess.PIPE
		popenkwargs['stderr'] = subprocess.PIPE

	if keepalive_interval <= 0:
		raise ValueError(f"'keepalive_interval' must be >0, '{keepalive_interval}' specified")

	if timeout is not None:
		if timeout <= 0:
			raise ValueError(f"'timeout' must be >0, '{timeout}' specified")
		if timeout < keepalive_interval:
			raise ValueError("`timeout` cannot be shorter than `keepalive_interval`")

	if len(keepalive_write) <= 0:
		raise ValueError("keepalive_write must not be empty")

	for fd in keepalive_write:
		if fd in keepalive_watch:
			raise ValueError("You can't have the same fd in `keepalive_watch` and `keepalive_write`")

	if (popenkwargs.get('stderr', None) == subprocess.STDOUT) and (WATCH_STDERR in keepalive_watch):
		raise ValueError("You're trying to watch stderr redirected to stdout")

	# >> handle WATCH_STD* constants
	stdouterr_pipings: typing.List[_StdouterrPiping] = list()
	for name, enum in (('stdout', WATCH_STDOUT), ('stderr', WATCH_STDERR)):
		if enum not in keepalive_watch:
			continue

		arg = popenkwargs.get(name, None)

		# > retrieve stdout/stderr file descriptors
		if arg is None:
			# get the global fds
			user_fd: int = getattr(sys, name).fileno()
		elif arg in {subprocess.DEVNULL, subprocess.PIPE}:
			raise ValueError(f"Watching '{name}' stream redirected to `/dev/null` or buffer makes no sense")
		elif isinstance(arg, int):
			# assuming it already is a fd
			user_fd: int = arg
		else:
			# assuming a file-like object
			user_fd: int = arg.fileno()

		# > create and register piping
		piping: _StdouterrPiping = _StdouterrPiping(user_fd)
		stdouterr_pipings.append(piping)

		# > spoof our pipe to Popen
		popenkwargs[name] = int(piping.proc.w)

	# these buffers will hold stdout+/stderr if user asked for it
	out_buffers: _Buffers = _Buffers()

	# > this will wakeup epoll_wait when the child exits
	chldexit_pipe = _Pipe()
	if popenkwargs.get('close_fds', True) is True:
		if 'pass_fds' in popenkwargs:
			popenkwargs['pass_fds'] = itertools.chain(popenkwargs['pass_fds'], (int(chldexit_pipe.w),))
		else:
			popenkwargs['pass_fds'] = (int(chldexit_pipe.w),)

	# >>> start the subprocess
	try:
		with subprocess.Popen(args, **popenkwargs) as process:
			# register process start time for the `timeout` feature
			proc_start_time: _Timestamp = _Timestamp()

			# timestamp of the last activity in the watched streams (either normal or ours)
			last_activity_ts: _Timestamp = copy.deepcopy(proc_start_time)

			# > close our view of writing ends of pipes so that EOF is passed correctly
			for p in stdouterr_pipings:
				p.proc.w.close()
			chldexit_pipe.w.close()

			with _Epoll() as epoll:
				epoll.register(int(chldexit_pipe.r), select.EPOLLHUP) \
					.with_handler(_ChildExitHandler, (process, ))

				# > register `input` handler
				if input:
					stdin_io: typing.IO = process.stdin
					epoll.register(stdin_io.fileno(), select.EPOLLOUT) \
						.with_handler(_InputHandler, (stdin_io, input))

				# > register stdout/stderr buffering handlers
				for name in ('stdout', 'stderr'):
					if popenkwargs.get(name, None) == subprocess.PIPE:
						fh = getattr(process, name)
						buf = getattr(out_buffers, name)
						epoll.register(fh.fileno(), select.EPOLLIN) \
							.with_handler(_BufferingHandler, (fh, buf))

				# > register stdout/stderr piping handlers
				for p in stdouterr_pipings:
					epoll.register(int(p.proc.r), select.EPOLLIN) \
						.with_handler(_StdouterrPipingHandler, (p, last_activity_ts))

				# > register non SUBPROC_* watchers
				for fd in keepalive_watch:
					if fd not in {WATCH_STDOUT, WATCH_STDERR}:
						epoll.register(fd, select.EPOLLIN) \
							.with_handler(_NonPipingWatcherHandler, (last_activity_ts,))

				# >> master loop
				while True:
					epoll_timeout: _Number = max(keepalive_interval - last_activity_ts.elapsed(), 0)

					try:
						events = epoll.poll(epoll_timeout)
					except InterruptedError:
						# just some OS-level interrupt we don't have to care about
						continue

					# > check and handle `timeout` feature
					if (timeout is not None) and (proc_start_time.elapsed() > float(timeout)):
						process.kill()
						stdout, stderr = out_buffers.get_all()
						raise subprocess.TimeoutExpired(process.args, timeout, output=stdout, stderr=stderr)

					if last_activity_ts.elapsed() > float(keepalive_interval):
						for fd in keepalive_write:
							# WARNING: this might block!
							os.write(fd, keepalive_packet)
						last_activity_ts.bump()

					# > epoll has data available, crunch them
					if events:
						for fd, event_types in events:
							epoll.dispatch_event(fd, event_types)

					# > epoll_wait() timed out
					else:
						pass

					if process.returncode is not None:
						for fd in epoll:
							if fd not in keepalive_watch:
								# any non-watching fd could still be waiting to be read, so continue polling
								break
						else:
							# no non-watching fd was found, we can end this
							break

				# >> end of master loop

	finally:
		for p in stdouterr_pipings:
			p.proc.close()

	retcode = process.poll()
	stdout, stderr = out_buffers.get_all()
	if check and retcode != 0:
		raise subprocess.CalledProcessError(retcode, process.args, output=stdout, stderr=stderr)

	return subprocess.CompletedProcess(process.args, retcode, stdout=stdout, stderr=stderr)
